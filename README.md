# Cron Fail Alert

The Cron Fail Alert module is a Drupal 8/9/10 module that alerts site
administrators when the Drupal cron has failed to run within a specified time
period. The module sends an email notification to the site administrator when
the Drupal cron has failed to run after a certain amount of time.

## Installation

1. Install and enable the module like any other Drupal module.
   See [Drupal documentation](https://www.drupal.org/docs/extending-drupal/installing-modules)
   for more information on installing Drupal modules.

2. After installation, navigate
   to `Configuration > System > Cron > Cron Fail Alert Settings` to configure
   the module settings.

3. Set the frequency and tolerance for the cron check. Frequency is how often
   the cron check should run in minutes. Tolerance is how many minutes should
   pass before the cron check is considered to have failed.

4. Set the email address to which notifications should be sent when the cron
   check fails.

5. Save the configuration.

## Usage

After installation and configuration, the module will run a cron check at the
specified frequency. If the cron has not run within the specified tolerance
period, an email notification will be sent to the specified email address.

## Contributing

Contributions to the module are welcome! If you encounter any issues or have any
suggestions for improvement, please open an issue or pull request on the
module's [repository](https://git.drupalcode.org/project/cron_fail_alert).

## License

This module is licensed under
the [GNU General Public License](https://www.gnu.org/licenses/gpl-2.0.html).
