<?php

namespace Drupal\cron_fail_alert\Form;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Cron fail alert settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The email validator.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected EmailValidatorInterface $emailValidator;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'cron_fail_alert_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['cron_fail_alert.settings'];
  }

  /**
   * Cron fail alert SettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, EmailValidatorInterface $email_validator) {
    parent::__construct($configFactory);
    $this->emailValidator = $email_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('email.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->configFactory->getEditable('cron_fail_alert.settings');
    $form['frequency'] = [
      '#type' => 'number',
      '#required' => TRUE,
      '#title' => $this->t('Frequency (in Minutes)'),
      '#description' => $this->t('This flag is necessary to prevent checks from being performed on every page load, and it specifies the frequency at which we want the checks to be run.'),
      '#default_value' => $config->get('frequency'),
      '#step' => 1,
      '#min' => 1,
    ];
    $form['tolerance'] = [
      '#type' => 'number',
      '#required' => TRUE,
      '#title' => $this->t('Tolerance (in Minutes)'),
      '#description' => $this->t("What's the duration of time we can permit before executing the cron job?") . '<br>' . $this->t('This value should exceed the frequency of Drupal cron, such as if Drupal cron runs every 15 minutes, this value could be set to 20.'),
      '#default_value' => $config->get('tolerance'),
    ];
    $form['to'] = [
      '#type' => 'email',
      '#required' => TRUE,
      '#title' => $this->t('(Send cron failure email) To'),
      '#description' => $this->t('This will override the value of the system.site.mail setting.'),
      '#default_value' => $config->get('to') ?? $this->configFactory->get('system.site')->get('mail'),
    ];
    $form['subject'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Subject'),
      '#description' => $this->t('The subject line of an email, this will include the site name by default at the end.'),
      '#default_value' => $config->get('subject'),
      '#maxlength' => 255,
    ];
    $form['message'] = [
      '#type' => 'textarea',
      '#required' => TRUE,
      '#title' => $this->t('Message'),
      '#description' => $this->t('The email content can make use of the parameters <code>@minutes</code> and <code>:site</code> to display the number of minutes since the last cron run and the corresponding site URL, respectively.'),
      '#default_value' => $config->get('message'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $email_address = $form_state->getValue('to');
    if (!$this->emailValidator->isValid($email_address)) {
      $form_state->setErrorByName('to', $this->t('The email address %mail is not valid.', ['%mail' => $email_address]));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('cron_fail_alert.settings');
    $config->set('frequency', $form_state->getValue('frequency'))
      ->set('tolerance', $form_state->getValue('tolerance'))
      ->set('to', $form_state->getValue('to'))
      ->set('subject', $form_state->getValue('subject'))
      ->set('message', $form_state->getValue('message'))
      ->save();
  }

}
