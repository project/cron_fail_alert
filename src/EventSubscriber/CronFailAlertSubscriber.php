<?php

namespace Drupal\cron_fail_alert\EventSubscriber;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Cron Fail Alert event subscriber.
 */
class CronFailAlertSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The cron fail alert settings object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $cronFailAlertSettings;

  /**
   * The site config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $systemSite;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected LoggerChannelFactoryInterface $loggerFactory;

  /**
   * The mail manager service.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected MailManagerInterface $mailManager;

  /**
   * The state key/value store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key/value store.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LanguageManagerInterface $language_manager, LoggerChannelFactoryInterface $logger_factory, MailManagerInterface $mail_manager, StateInterface $state, RequestStack $request_stack) {
    $this->cronFailAlertSettings = $config_factory->get('cron_fail_alert.settings');
    $this->systemSite = $config_factory->get('system.site');
    $this->languageManager = $language_manager;
    $this->loggerFactory = $logger_factory;
    $this->mailManager = $mail_manager;
    $this->state = $state;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::RESPONSE => ['onKernelResponse'],
    ];
  }

  /**
   * Runs check on the cron job status and sends email alerts if it has failed.
   */
  public function onKernelResponse(): void {
    // Define the frequency of checks in minutes.
    $frequency = $this->cronFailAlertSettings->get('frequency');

    // Retrieve the timestamp of the last check.
    $lastCheckTimestamp = $this->state->get('cron_fail_alert.last_check_timestamp', 0);

    // Calculate the time passed since the last check in minutes.
    $minutesSinceLastCheck = (time() - $lastCheckTimestamp) / 60;

    // Proceed only if the time elapsed since the last check exceeds the
    // specified frequency.
    if ($minutesSinceLastCheck < $frequency) {
      return;
    }

    // Perform the cron status check.
    if ($this->checkCronStatus()) {
      // The cron got failed. Update the 'last check timestamp' for the next
      // check.
      $this->state->set('cron_fail_alert.last_check_timestamp', time());
    }
  }

  /**
   * Compares last cron run time with a set tolerance to check for any failures.
   *
   * @return bool
   *   Returns TRUE if the cron has failed, FALSE otherwise.
   */
  protected function checkCronStatus(): bool {
    // Determine the time elapsed since the last cron job execution.
    $lastCron = $this->state->get('system.cron_last');
    $minutesAgo = (time() - $lastCron) / 60;
    $tolerance = $this->cronFailAlertSettings->get('tolerance');

    // Compare elapsed time with tolerance to identify cron failure.
    if (!$minutesAgo >= $tolerance) {
      // Return FALSE if there is no indication of cron failure.
      return FALSE;
    }
    // Cron appears to have failed; prepare to send an email alert.
    $to = $this->cronFailAlertSettings->get('to') ?? $this->systemSite->get('mail');
    $subject = new FormattableMarkup('@subject on Site: @site_name', [
      '@subject' => $this->cronFailAlertSettings->get('subject'),
      '@site_name' => $this->systemSite->get('name'),
    ]);
    $message = new FormattableMarkup($this->cronFailAlertSettings->get('message'), [
      '@minutes' => floor($minutesAgo),
      ':site' => $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost(),
    ]);
    $this->sendCronFailAlertEmail($to, $subject, $message);
    // Indicate that cron has failed by returning TRUE.
    return TRUE;
  }

  /**
   * Sends an email notification to the specified recipient.
   *
   * @param string $to
   *   The email address of the recipient.
   * @param string $subject
   *   The subject of the email.
   * @param string $body
   *   The body of the email.
   *
   * @return bool
   *   TRUE if the email was sent successfully, otherwise FALSE.
   */
  protected function sendCronFailAlertEmail(string $to, string $subject, string $body): bool {
    // Set the module and key values to identify the email template to use.
    $module = 'cron_fail_alert';
    $key = 'cron_fail_alert_mail';

    // Set the email parameters to be used in the email template.
    $params = [
      'title' => $subject,
      'message' => $body,
    ];

    // Get the default language code.
    $langcode = $this->languageManager->getDefaultLanguage()->getId();

    // Build the email and attempt to send it.
    $result = $this->mailManager->mail($module, $key, $to, $langcode, $params);

    // Check if the email was sent successfully.
    if (!$result['result']) {
      // If sending the email failed, log an error message.
      $errorMessage = $this->t('Failed to send email notification to @email.', ['@email' => $to]);
      $this->loggerFactory->get('cron_fail_alert')->error($errorMessage);
      return FALSE;
    }

    // If the email was sent successfully, log a success message.
    $successMessage = $this->t('Email notification sent successfully to @email', ['@email' => $to]);
    $this->loggerFactory->get('cron_fail_alert')->notice($successMessage);

    // Return TRUE to indicate that the email was sent successfully.
    return TRUE;
  }

}
